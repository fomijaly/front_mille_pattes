import React from "react";
import ReactDOM from "react-dom/client";

import { 
  createBrowserRouter, 
  RouterProvider 
} from "react-router-dom";

import Navigation from "./components/Navigation";

import "./index.css";
import Home from "./pages/home/Home";
import AnimalList from "./pages/details/AnimalList";
import AnimalOwners from "./pages/details/AnimalOwners";
import MedicineList from "./pages/details/MedicineList";
import MemberList from "./pages/details/MemberList";

const router = createBrowserRouter([
  {
    path: "/",
    element: <Navigation/>,
    children: [
      {
        path: '/',
        element: <Home/>,
      },
      {
        path: '/animals',
        element: <AnimalList/>,
      },
      {
        path: '/animal_owners',
        element: <AnimalOwners />,
      },
      {
        path: '/medicines',
        element: <MedicineList/>,
      },
      {
        path: '/members',
        element: <MemberList />,
      }
    ],
  }
])

ReactDOM.createRoot(document.getElementById("root")).render(
  <React.StrictMode>
    <RouterProvider router={router}/>
  </React.StrictMode>
);
