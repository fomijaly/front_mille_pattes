import "./RegisterAnimal.css";
import { useState, useEffect } from "react";
import axios from "axios";

function RegisterAnimal() {
  const [animal_name, setAnimalName] = useState("");
  const [animal_birthday, setAnimalBirthday] = useState("");
  const [animal_specie, setAnimalSpecie] = useState("");
  const [animal_description, setAnimalDescription] = useState("");
  const [animal_owner_id, setAnimalOwner] = useState(0);
  const [animal_picture, setAnimalPicture] = useState();

  const handleSubmit = async (e) => {
    e.preventDefault();
    const formData = new FormData();
    formData.append("name", animal_name);
    formData.append("birthday", animal_birthday);
    formData.append("specie", animal_specie);
    formData.append("description", animal_description);
    formData.append("animal_owner_id", animal_owner_id);
    formData.append("animal_picture", animal_picture);

    await axios
      .post("http://localhost:3000/animals", formData, {
        headers: {
          "Content-Type": "multipart/form-data",
        },
      })
      .then((response) => console.log(response))
      .catch((error) => console.log(error));

    setAnimalName("");
    setAnimalBirthday("");
    setAnimalSpecie("");
    setAnimalDescription("");
    setAnimalOwner(0);
    setAnimalPicture();
    window.location.reload();
  };

  return (
    <article id="register_pet">
      <h2>Enregistrez votre compagnon</h2>
      <form className="form_register-animal" onSubmit={handleSubmit}>
        <label>
          Votre compagnon s'appelle : <br />
          <input
            className="form-control"
            type="text"
            value={animal_name}
            onChange={(e) => setAnimalName(e.target.value)}
          />
        </label>
        <label>
          Il est né le : <br />
          <input
            className="form-control"
            type="date"
            value={animal_birthday}
            onChange={(e) => setAnimalBirthday(e.target.value)}
          />
        </label>
        <label>
          Quelle est son espèce ? <br />
          <select
            value={animal_specie}
            onChange={(e) => setAnimalSpecie(e.target.value)}
          >
            <option value="">--</option>
            <option value="chat">Chat</option>
            <option value="chien">Chien</option>
            <option value="lapin">Lapin</option>
            <option value="souris">Souris</option>
            <option value="cochonInde">Cochon d'Inde</option>
            <option value="hamster">Hamster</option>
            <option value="furet">Furet</option>
            <option value="rat">Rat</option>
            <option value="gerbille">Gerbille</option>
            <option value="araignee">Araignée</option>
            <option value="cameleon">Caméléon</option>
            <option value="lezard">Lézard</option>
          </select>
        </label>
        <label>
          Pouvez-vous le décrire ? <br />
          <textarea
            className="form-control"
            type="text"
            value={animal_description}
            onChange={(e) => setAnimalDescription(e.target.value)}
          />
        </label>
        <label>
          Télécharger une belle image : <br />
          <input
            onChange={(e) => setAnimalPicture(e.target.files[0])}
            type="file"
            encType="multipart/form-data"
            name="animal_picture"
          />
        </label>
        <label>
          Indiquez votre identifiant de propriétaire :
          <input
            type="number"
            value={animal_owner_id}
            onChange={(e) => setAnimalOwner(e.target.value)}
          />
        </label>
        <button type="submit" className="btn main_button">
          Enregistrer votre compagnon
        </button>
      </form>
    </article>
  );
}

export default RegisterAnimal;
