function Member(props) {
  const { ...member } = props;
  return (
    <>
      <li>
        <span>{member.firstName}</span>
      </li>
    </>
  );
}

export default Member;
