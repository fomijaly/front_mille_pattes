import { useEffect } from "react";
import "./Animal.css";
import ReactModal from "react-modal";

function Animal(props) {
  const { ...animal } = props;
  const birthday = animal.birthday.split("T");
  const birthdayDate = birthday[0];

  function emoji() {
    const species = animal.specie;
    switch (species) {
      case "chat":
        return "🐱";
      case "chien":
        return "🐶";
      case "lapin":
        return "🐰";
      case "souris":
        return "🐭";
      case "cochonInde":
        return "Cochon d'Inde";
      case "hamster":
        return "🐹";
      case "furet":
        return "Furet";
      case "rat":
        return "🐀";
      case "gerbille":
        return "Gerbille";
      case "araignee":
        return "🕷️";
      case "cameleon":
        return "Caméléon";
      case "lezard":
        return "🦎";
      case "":
        return "❔";
      default:
        return "animal";
    }
  }

  return (
    <li className="card card_animal">
      <img
        src={`http://localhost:5173/images/${animal.animal_picture}`}
        alt={`Photo de ${animal.name}`}
      />
      <span className="animal_type">{emoji()}</span>
      <div className="overlay">
        <div className="card_animal-details">
          <h4>{animal.name}</h4>
          <span>
            <strong>Naissance : </strong>
            {birthdayDate}
          </span>
          <span>
            <strong>Propriétaire : </strong>
            {`${animal.firstName} ${animal.lastName}`}
          </span>
          <div className="description_animal">
            <p>{animal.description}</p>
          </div>
        </div>
      </div>
    </li>
  );
}

export default Animal;
