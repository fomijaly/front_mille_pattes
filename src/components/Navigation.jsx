import { Outlet, NavLink } from "react-router-dom";
import "./Navigation.css";
import Logo from "../assets/logo.png"

function Navigation() {
  return (
    <>
      <nav className="navigation">
        <span className="navigation_logo">
          <img src={Logo} alt="Logo Milles pattes" />
        </span>
        <ul className="navigation_menu">
          <li>
            <NavLink to={`/`}>Accueil</NavLink>
          </li>
          <li>
            <NavLink to={`/animals`}>Animaux</NavLink>
          </li>
          <li>
            <NavLink to={`/animal_owners`}>Propriétaires</NavLink>
          </li>
          <li>
            <NavLink to={`/medicines`}>Médicaments</NavLink>
          </li>
          <li>
            <NavLink to={`/members`}>Membres</NavLink>
          </li>
          <div className="log_buttons">
            <button className="btn btn-main-outline">Log in</button>
            <button className="btn main_button">Sign up</button>
          </div>
        </ul>
      </nav>
      <div>
        <Outlet />
      </div>
    </>
  );
}

export default Navigation;
