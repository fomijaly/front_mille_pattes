import { useEffect, useState } from "react";
import "./AnimalOwners.css";
import axios from "axios";
import Owner from "../../components/Owner";

function AnimalOwners() {
  const [ownerList, setOwnerList] = useState([]);
  useEffect(() => {
    axios.get("http://localhost:3000/animal_owners").then((response) => {
      setOwnerList(response.data);
    });
  }, []);

  return (
    <>
      <article>
        <h1>Propriétaires</h1>
        <ul>
          {ownerList.map((owner) => (
            <Owner
              key={owner.animal_owner_id}
              firstName={owner.firstName}
              lastName={owner.lastName}
            />
          ))}
        </ul>
      </article>
    </>
  );
}

export default AnimalOwners;
