import { useState } from "react";
import "./MedicineList.css";
import { useEffect } from "react";
import axios from "axios";
import Medicine from "../../components/Medicine";

function MedicineList() {
  const [medicineList, setmedicineList] = useState([]);
  useEffect(() => {
    axios.get("http://localhost:3000/medicines").then((response) => {
      setmedicineList(response.data);
    });
  }, []);
  return (
    <>
      <article>
        <h1>Liste de médicaments</h1>
        <ul>
          {medicineList.map((medicine) => (
            <Medicine key={medicine.medicine_id} {...medicine} />
          ))}
        </ul>
      </article>
    </>
  );
}

export default MedicineList;
