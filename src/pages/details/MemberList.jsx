import { useState, useEffect } from "react";
import "./MemberList.css";
import axios from "axios";
import Member from "../../components/Member";

function MemberList() {
  const [memberList, setmemberList] = useState([]);
  useEffect(() => {
    axios.get("http://localhost:3000/members").then((response) => {
      setmemberList(response.data);
    });
  }, []);

  return (
    <>
      <article>
        <h1>Collaborateurs</h1>
        <ul>
          {memberList.map((member) => (
            <Member key={member.member_id} {...member} />
          ))}
        </ul>
      </article>
    </>
  );
}

export default MemberList;
