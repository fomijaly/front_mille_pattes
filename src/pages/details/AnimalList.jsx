import { useState, useEffect } from "react";
import axios from "axios";
import "./AnimalList.css";
import Animal from "../../components/Animal";
import RegisterAnimal from "../../components/RegisterAnimal";

function AnimalList() {
  const [animalList, setAnimalList] = useState([]);
  const [filteredAnimalList, setFilteredAnimalList] = useState([]);
  const [selectedButton, setSelectedButton] = useState(null)

  useEffect(() => {
    axios.get("http://localhost:3000/animals").then((response) => {
      setAnimalList(response.data);
      setFilteredAnimalList(response.data);
    });
  }, []);

  const filterSpecies = (specie) => {
    const newFilter = animalList.filter((animal) => animal.specie === specie);
    if(newFilter.length > 0){
      setFilteredAnimalList(newFilter);
      setSelectedButton(specie);
    } else {
      alert("Il n'y a rien ici pour le moment");
    }
  };

  return (
    <main>
      <section>
        <article>
          <h1>Liste des animaux</h1>
          <ul className="buttons_filter">
            <button
              className={`btn ${selectedButton === null ? 'main_button' : 'btn-main-outline '}`}
              onClick={() => {setFilteredAnimalList(animalList); setSelectedButton(null);}}
            >
              Tous
            </button>
            <button
              className={`btn ${selectedButton === "chat" ? 'main_button' : 'btn-main-outline '}`}
              onClick={() => filterSpecies("chat")}
            >
              Chat
            </button>
            <button
              className={`btn ${selectedButton === "chien" ? 'main_button' : 'btn-main-outline '}`}
              onClick={() => filterSpecies("chien")}
            >
              Chien
            </button>
            <button
              className={`btn ${selectedButton === "lapin" ? 'main_button' : 'btn-main-outline '}`}
              onClick={() => filterSpecies("lapin")}
            >
              Lapin
            </button>
            <button
              className={`btn ${selectedButton === "souris" ? 'main_button' : 'btn-main-outline '}`}
              onClick={() => filterSpecies("souris")}
            >
              Souris
            </button>
            <button
              className={`btn ${selectedButton === "cochonInde" ? 'main_button' : 'btn-main-outline '}`}
              onClick={() => filterSpecies("cochonInde")}
            >
              Cochon d'Inde
            </button>
            <button
              className={`btn ${selectedButton === "hamster" ? 'main_button' : 'btn-main-outline '}`}
              onClick={() => filterSpecies("hamster")}
            >
              Hamster
            </button>
            <button
              className={`btn ${selectedButton === "furet" ? 'main_button' : 'btn-main-outline '}`}
              onClick={() => filterSpecies("furet")}
            >
              Furet
            </button>
            <button
              className={`btn ${selectedButton === "rat" ? 'main_button' : 'btn-main-outline '}`}
              onClick={() => filterSpecies("rat")}
            >
              Rat
            </button>
            <button
              className={`btn ${selectedButton === "gerbille" ? 'main_button' : 'btn-main-outline '}`}
              onClick={() => filterSpecies("gerbille")}
            >
              Gerbille
            </button>
            <button
              className={`btn ${selectedButton === "araignee" ? 'main_button' : 'btn-main-outline '}`}
              onClick={() => filterSpecies("araignee")}
            >
              Araignée
            </button>
            <button
              className={`btn ${selectedButton === "cameleon" ? 'main_button' : 'btn-main-outline '}`}
              onClick={() => filterSpecies("cameleon")}
            >
              Caméléon
            </button>
            <button
              className={`btn ${selectedButton === "lezard" ? 'main_button' : 'btn-main-outline '}`}
              onClick={() => filterSpecies("lezard")}
            >
              Lézard
            </button>
            <button
              className={`btn ${selectedButton === "" ? 'main_button' : 'btn-main-outline '}`}
              onClick={() => filterSpecies("")}
            >
              Autre
            </button>
          </ul>

          <ul className="list_pets">
            {filteredAnimalList.map((animal) => (
              <Animal key={animal.animal_id} {...animal} />
            ))}
          </ul>
        </article>
      </section>
      <section className="register_animal">
        <RegisterAnimal />
      </section>
    </main>
  );
}

export default AnimalList;
