import "./Home.css";
import { HashLink as NavLink} from "react-router-hash-link";

function Home() {
  return (
    <main className="home">
      <section className="home_main-content">
        <article className="home_header">
          <div className="home_main-title">
            <h1>Bienvenue chez Mille Pattes</h1>
            <span>
              votre partenaire dévoué pour la santé et le bien-être de vos
              compagnons à quatre pattes !
            </span>
            <NavLink to={"/animals#register_pet"}>
              <button className="btn main_button">
                Enregistrer votre compagnon
              </button>
            </NavLink>
          </div>
          <div className="home_main-img">
            <div className="home_img-bloc">
              <img
                className="home_image-green"
                src="http://tinyurl.com/2p8n4hs6"
                alt=""
              />
            </div>
            <div className="home_img-bloc">
              <img
                className="home_image-white-dog"
                src="http://tinyurl.com/56sy5yda"
                alt=""
              />
              <img
                className="home_image-blue"
                src="http://tinyurl.com/ycj6kje2"
                alt=""
              />
            </div>
            <div className="home_img-bloc">
              <img
                className="home_image-red"
                src="http://tinyurl.com/4ka6sa35"
                alt=""
              />
              <img
                className="home_image-white-cat"
                src="http://tinyurl.com/2s3hxtfu"
                alt=""
              />
            </div>
          </div>
        </article>
      </section>
      <section className="home_content">
        <img
          className="home_content-img"
          src="http://tinyurl.com/2buz69bs"
          alt=""
        />
        <div className="home_content-description">
          <div className="home_detail-description">
            <h2>
              Des soins exceptionnels <br /> pour vos précieux compagnons
            </h2>
            <p>
              Notre équipe expérimentée est là pour répondre à tous leurs
              besoins de santé, que ce soit pour des examens de routine, des
              vaccinations, des chirurgies ou des urgences.
            </p>
            <p>
              Notre environnement chaleureux favorise le bien-être de vos
              animaux et votre tranquillité d'esprit. Chez Mille Pattes, nous
              sommes passionnés par la santé animale et nous sommes là pour
              accompagner le bonheur et la vitalité de vos amis à quatre pattes
            </p>
          </div>
        </div>
      </section>
      <section>
        <article>
          Lorem ipsum dolor sit amet consectetur, adipisicing elit. Aspernatur
          mollitia et enim doloribus sit animi tempora? Animi, maiores adipisci?
          Obcaecati dignissimos et quo sapiente inventore illo cumque molestias
          id numquam.
        </article>
      </section>
    </main>
  );
}

export default Home;
